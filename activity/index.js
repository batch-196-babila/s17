/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    let userDetails = function() {
        const userFullName = prompt("Enter your Fullname:");
        const userAge = prompt("Enter your Age:");
        const userLocation = prompt("Enter your Location:");

        console.log("Hi " + userFullName);
        console.log(userFullName + "'s " + "age is " + userAge);
        console.log(userFullName + " is located at " + userLocation);
    };

    userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.d
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    let favoriteArtists = function topFiveArtists() {
        let favoriteMusicArtists = ["Ariana Grande", "Nicki Minaj", "The Weeknd", "Shawn Mendes", "Morisette Amon",];
        console.log(favoriteMusicArtists);
    };

    favoriteArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    let favoriteMovies = function topFiveMovies() {
        console.log("1. WandaVision\nTomatometer for WandaVision: 91%");
        console.log("2. Doctor Strange 2: MoM\nTomatometer for Doctor Strange 2: MoM: 74%");
        console.log("3. Loki\nTomatometer for Loki: 92%");
        console.log("4. Shang Chi\nTomatometer for Shang Chi: 91%");
        console.log("5. Eternals\nTomatometer for Eternals: 47%");
    };

    favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1)
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
// console.log(friend3);