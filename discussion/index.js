/* 
FUNCTION

Syntax
    function functionName () {
        code block(statement);
    }
*/

function printName() {
    console.log("My name is Anna");
};

printName(); // invoke / call the function

// declaredFunction(); // error

/* FUNCTION DECLARATION VS EXPRESSION */

declaredFunction();

function declaredFunction() {
    console.log("Hi! I am from declaredFunction()");
}; // unable to use as variable

declaredFunction();

// FUNCTION EXPRESSION
// Anonymous function - function without a name
// need to declare function before calling

let variableFunction = function() {
    console.log("I am from variableFunction");
};

variableFunction();

let funcExpression = function funcName() {
    console.log("Hello from the other side from funcExpression()");
};

funcExpression(); // call via variable name

// You can reassign declared function and function expression to new anonymouse function

declaredFunction = function() {
    // converted basic to expression function
    console.log("Updated declaredFunction"); 
};

declaredFunction();

funcExpression = function() {
    console.log("Updated funcExpression")
};

funcExpression();
const constantFunction = function() {
    console.log("Initialized with const");
}

constantFunction();

// constantFunction = funtion() {
//     console.log("Cannot be reassigned");
// };

// constantFunction();

// reassignmetn with const function expression is not possible - error

/* 
    FUNCTION SCOPING 
    JS Variables has 3 types of Scope
    1. local/block scope
    2. global scope
    3. function scope
*/

{
    let localVar = "Local scope";
}

let globalVar = "global scope";

console.log(globalVar);
// console.log(localVar); // err


function showNames() {
    var functionVar = "Joe";
    const functionConst = "Nick";
    let functionLet = "Kevin";
    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
};

showNames();

// Result: ERROR
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

/* NESTED FUNCTIONS */

console.log("Nested Function");

function myNewFunction() {
    let name = "Yor";

    function nestedFunction() {
        let nestedName = "Brando";
        console.log(nestedName);
    };
    nestedFunction(); // need to invoke to work
};
myNewFunction();

// FUNCTION AND GLOBAL SCORE VARIABLE

// global scored variable

let globalName = "Alan";

function myNewFunction2() {
    let nameInside = "Marco";
    console.log(globalName);
}; 
myNewFunction2();

// alert()
/*
alert("haha");

function showSampleAlert() {
    alert("Alert inside a function");
};

showSampleAlert();

console.log("I will only log in the console when the alert is dismissed");
*/

// prompt()
// Syntax: prompt("<dialog>")

/*

let samplePrompt = prompt("Enter your name");
console.log("Hello " + samplePrompt);

let sampleNullPrompt = prompt("Don't enter anything");
console.log(sampleNullPrompt); // click cancel = null

*/

function printWelcomeMessage() {
    let firstname = prompt("Enter your first name: ");
    let lastname = prompt("Enter your last name: ");
    console.log("Hello, " + firstname + " " + lastname + "!");
    console.log("Welcome to my page");
};

// printWelcomeMessage();

// FUNCTION NAMING CONVENTION

function getCourses() {
    let courses = ['Science', 'Math', 'English'];
    console.log(courses);
};

getCourses();

function foo() {
    console.log(25 % 5);
} 
foo();

// follow camel case